<?php

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\AuthenticationService;

class Module {

    public function onBootstrap(MvcEvent $e) {
        $this->initAcl($e);
        $e->getApplication()->getEventManager()->attach('route', array($this, 'checkAcl'));
    }

    public function initAcl(MvcEvent $e) {


        $acl = new \Zend\Permissions\Acl\Acl();

        $acl->addRole(new \Zend\Permissions\Acl\Role\GenericRole('guest'))
                ->addRole(new \Zend\Permissions\Acl\Role\GenericRole('administrator'));

        $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource('home'));

        $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource('login'));

        $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource('user'));

        $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource('album'));

        $acl->allow('guest', array('home', 'login'));
        $acl->allow('administrator', null);
        
//        foreach ($roles as $role => $resources) {
//            $role = new \Zend\Permissions\Acl\Role\GenericRole($role);
//            $acl->addRole($role);
//
//            $allResources = array_merge($resources, $allResources);
//
//
//            foreach ($resources as $resource) {
//                $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
//            }
//
//            foreach ($allResources as $resource) {
//                $acl->allow($role, $resource);
//            }
//        }
        //testing
        //var_dump($acl->isAllowed('guest','user'));
        //die();
//true
        //setting to view
        
        
        $e->getViewModel()->acl = $acl;
    }

    public function checkAcl(MvcEvent $e) {
        $route = $e->getRouteMatch()->getMatchedRouteName();

        $auth = new AuthenticationService();

        if ($auth->hasIdentity()) {
            $userRole = 'administrator';
        } else {
            $userRole = 'guest';
        }

//        print_r($e -> getViewModel() -> acl);
//        die();
        if (!$e->getViewModel()->acl->isAllowed($userRole, $route)) {
            $response = $e->getResponse();
            $response->getHeaders()->addHeaderLine('Location', $e->getRequest()->getBaseUrl() . '/404');
            $response -> setStatusCode(303);

        }
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}
